import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Icon from '@material-ui/core/Icon';
export default function GroupedButtons() {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <ButtonGroup fullWidth aria-label="full width outlined button group">
          <Button>
            <Icon>add</Icon>
            Ingresos
          </Button>
          <Button>
            <Icon>remove</Icon>Gastos
          </Button>
          <Button>
            {' '}
            <Icon>shuffle</Icon>Transferencias
          </Button>
        </ButtonGroup>
      </Grid>
    </Grid>
  );
}
