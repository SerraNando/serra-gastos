import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      marginTop: theme.spacing(3),
      overflowX: 'auto'
    },
    table: {
      minWidth: 80
    }
  })
);

function createData(name: string, sum: number) {
  return { name, sum };
}

const rows = [
  createData('Ingreso:', 159),
  createData('Remanentes:', 237),
  createData('Gastos:', 262),
  createData('Total:', 305)
];

export default function SimpleTable() {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell align="right">Monto</TableCell>
            {/* <TableCell align="right">Remnants&nbsp;(g)</TableCell>
            <TableCell align="right">outlay&nbsp;(g)</TableCell>
            <TableCell align="right">Total&nbsp;(g)</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {console.log(row)}
                {row.name}
              </TableCell>
              <TableCell align="right">{row.sum}</TableCell>
              {/* <TableCell align="right">{row.Remnants}</TableCell>
              <TableCell align="right">{row.outlay}</TableCell>
              <TableCell align="right">{row.Total}</TableCell> */}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}
