import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Table from '../Table';
const useStyles = makeStyles(
  createStyles({
    card: {
      minWidth: 275
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)'
    },
    title: {
      fontSize: 14
    },
    pos: {
      marginBottom: 12
    }
  })
);

export default function SimpleCard() {
  const classes = useStyles();
  // const bull = <span className={classes.bullet}>•</span>;

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Card className={classes.card}>
          <CardContent>
            {/* <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
            >
             Balance mensual + remanentes 
            </Typography> */}
            <Typography variant="h5" component="h2">
              Balance mensual + remanentes
              {/* be
              {bull}
              nev
              {bull}o{bull}
              lent */}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              Julio 2019
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Table />
                {/* <Typography variant="body2" component="p">
                  Ingreso:
                  <br />
                  Remanentes:
                  <br />
                  Gastos:
                  <br />
                  <br />
                  Total:
                </Typography>
              </Grid>
              <Grid item xs={6} sm={6}>
                <Typography variant="body2" component="p">
                  1.700.000
                  <br />
                  50.0000
                  <br />
                  1.250.000
                  <br />
                  <br />
                  500.000
                </Typography> */}
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <Button size="small">Learn More</Button>
          </CardActions>
        </Card>
      </Grid>
    </Grid>
  );
}
