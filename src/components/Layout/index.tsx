import React, { FC, Fragment } from 'react';
import { Helmet } from 'react-helmet';
import { Div, Title, Subtitle } from './styles';
import AppBar from '../AppBar';

interface Props {
  subtitle: any;
  title: any;
}

export const Layout: FC<Props> = ({ children, subtitle, title }) => (
  <Fragment>
    <Helmet>
      {title && <title>{title} | Gastos </title>}
      {subtitle && <meta name="description" content={subtitle} />}
    </Helmet>
    {/* <Div>
      {title && <Title>{title}</Title>}
      {subtitle && <Subtitle>{subtitle}</Subtitle>}
      {children}
    </Div> */}
    <AppBar title={title} />
    {children}
  </Fragment>
);
