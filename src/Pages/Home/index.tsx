import React, { FC } from 'react';
import { Layout } from '../../components/Layout';
import SimpleSelect from '../../components/Select';
import ButtonGroup from '../../components/GroupedButtons';
import Card from '../../components/Card';
export const Home: FC = () => {
  return (
    <Layout title="HomePage" subtitle="Titulo de home page">
      <SimpleSelect />
      <ButtonGroup />
      <Card />
    </Layout>
  );
};
