import React, { Fragment } from 'react';
import CssBaseLIne from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
// import AppBar from './components/AppBar';
import { Home } from './Pages/Home';

const App: React.FC = () => {
  return (
    <Fragment>
      <CssBaseLIne />
      <Container fixed>
        {/* <AppBar /> */}
        <Home />
      </Container>
    </Fragment>
  );
};
export default App;
